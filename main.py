import serial
import os
from os import path
from arrow import Arrow

conn = serial.Serial()
conn.baudrate= '9600'

conn.port = 'COM3'

out_dir = path.join(path.dirname(__file__) , 'out')

def main():
    with conn as ser:
        with open(path.join(out_dir, 'data.csv'), 'a') as f:
            line = ""
            while True:
                char = ser.read().decode('utf-8')
                print(char, end='', flush=True)
                line += char
                if char == "\n":
                    date = Arrow.now().format('DD/MM/YY hh:mm:ss')
                    line = date + '\t' + line
                    f.write(line.replace('\t', ','))
                    f.flush()
                    line = ''

if __name__ == '__main__':
    try:
        os.mkdir(out_dir)
    except FileExistsError:
        pass

    main()
